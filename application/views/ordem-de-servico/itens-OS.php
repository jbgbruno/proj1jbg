

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ordem de Serviço
        <small>Nova Entrada</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Itens da O.S</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">


                        <!-- Nav tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" role="tablist">

                                <li id ="" role="presentation" class="active"><a href="#servicos" aria-controls="servicos" role="tab" data-toggle="tab"><strong>Serviços</strong></a></li>
                                <li role="presentation"><a href="#produtos" aria-controls="produtos" role="tab" data-toggle="tab"><strong>Produtos</strong></a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane  active" id="servicos">
                                    <div class="row">
                                        <br>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Buscar Serviço:</label> 
                                                <input type="text" class="form-control input-sm" name="nome" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label style="color: #fff">&nbsp; &nbsp;</label>
                                                <br>
                                                <button class="btn btn-primary btn-sm">
                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"> <strong>Adcionar</strong></span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <table class="table table-bordered table-striped">
                                                <tbody><tr class="bg-gray">
                                                        <th style="width: 10px">#</th>
                                                        <th>Serviço</th>

                                                        <th class="text-center col-md-1"> Ações </th>
                                                        <th class="text-center col-md-2"> Subtotal </th>
                                                    </tr>
                                                    <tr>
                                                        <td class= " text-center">1.</td>
                                                        <td>Update software</td>
                                                        <td class=" text-center">
                                                            <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                        </td>
                                                        <td class=" text-center">150,00</td>
                                                    </tr>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                                        <td><strong>R$ 50,00</strong></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="produtos">
                                    <div class="row">
                                        <br>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Buscar Produto:</label> 
                                                <input type="text" class="form-control input-sm" name="nome" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Quantidade:</label> 
                                                <input type="text" class="form-control input-sm" name="qnt" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label style="color: #fff">&nbsp; &nbsp;</label>
                                                <br>
                                                <button class="btn btn-primary btn-sm">
                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"> <strong>Adcionar</strong></span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <table class="table table-bordered table-striped">
                                                <tbody><tr class="bg-gray">
                                                        <th style="width: 10px">#</th>
                                                        <th>Produtos</th>
                                                        <th class="text-center col-md-2">Quantidade</th>

                                                        <th class="text-center col-md-1"> Ações </th>
                                                        <th class="text-center col-md-2"> Subtotal </th>
                                                    </tr>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td>Update software</td>
                                                        <td class=" text-center">2</td>
                                                        <td class=" text-center">
                                                            <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                        </td>
                                                        <td class=" text-center">150,00</td>
                                                    </tr>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right"><strong>Total:</strong></td>
                                                        <td><strong>R$ 50,00</strong></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class=" btn btn-success">Finalizar</button>
                        <button type="button" class="btn btn-danger">Cancelar O.S</button>

                    </div>
            </div>
            </form>
        </div><!-- /.box -->

    </div>
</div><!-- /.row -->
<!-- Main row -->


</section><!-- /.content -->
