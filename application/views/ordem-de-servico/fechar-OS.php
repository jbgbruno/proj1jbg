
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ordem de Serviço
        <small>Nova Entrada</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Fechar Ordem de Serviço</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">


                    </div><!-- /.box -->
                    <div class="box-footer">
                        <button type="submit" class=" btn btn-success" >Fechar</button>

                        <button type="button" class="btn btn-primary">Salvar</button>
                        <button type="button" class="btn btn-danger">Cancelar O.S</button>
                </form>    
                    </div>

            </div>
        </div><!-- /.row -->
        <!-- Main row -->


</section><!-- /.content -->
