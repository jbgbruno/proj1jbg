
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ordem de Serviço
        <small>Nova Entrada</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Detalhes da O.S</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">

                        <form action="" name="cad_clientes" method="post">
                            <br>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Codigo:</label> 
                                        <input type="text" class="form-control input-sm " name="cod" value="">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Cliente:</label> 
                                        <input type="text" class="form-control input-sm" name="nome" value="">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Tecnico/Responsavel:</label> 
                                        <input type="text" class="form-control input-sm"  name="tecnico" value="">
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label>Status:</label> 
                                        <select class="form-control input-sm">
                                            <option>Aberta</option>
                                            <option>Em Andamento</option>
                                            <option>Finalizada</option>
                                            <option>Cancelada</option>

                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Data Inicial:</label> 
                                        <input type="text" class="form-control input-sm" name="endereco" value="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Data Final:</label> 
                                        <input type="text" class="form-control input-sm" name="endereco" value="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Garantia:</label> 
                                        <input type="text" class="form-control input-sm" name="endereco" value="">
                                    </div>
                                </div>
                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label>Tipo de Pagamento:</label> 
                                        <select class="form-control input-sm">
                                            <option>À vista</option>
                                            <option>Prazo</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label>Prazo:</label> 
                                        <select class="form-control input-sm">
                                            <option>1x</option>
                                            <option>2x</option>
                                            <option>3x</option>

                                        </select>

                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Observações do Cliente:</label>
                                        <textarea class="form-control input-sm" name="obs_cliente" rows="6"></textarea>
                                    </div>
                                </div>
                                
                            </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class=" btn btn-primary" >Iniciar</button>

                        <button type="button" class="btn btn-danger">Voltar</button>

                    </div>
            </div>
            </form>
        </div><!-- /.box -->

    </div>
</div><!-- /.row -->
<!-- Main row -->


</section><!-- /.content -->