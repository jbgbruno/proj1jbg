<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Dados Gerados</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-ticket"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Ordens de Serviço</span>
                    <span class="info-box-number">1000</span>
                </div><!-- /.info-box-content -->

            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-orange-active"><i class="fa fa-shopping-cart"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Vendas de Produtos</span>
                    <span class="info-box-number">2000</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-cash"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Contas a Receber</span>
                    <span class="info-box-number">2,000</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class= "fa ion-social-usd"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Contas a Pagar</span>
                    <span class="info-box-number">760</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary  ">
                <div class="box-header with-border">
                    <h3 class="box-title">Produtos com saldo baixo</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">



                    </div><!-- /.box-body -->


            </div>
        </div>
    </div>

</section><!-- /.content -->
