

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Produtos
        <small>Cadastro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Produtos</h3>

                </div><!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">



                    <div class="row">
                        <div class="col-md-12">
<?php

if (isset($_POST['salvar'])) {
    require('../_app/config.inc.php');
    $descricao = (string) $_POST['descricao'];
    $unidade = (string) $_POST['unidade'];
    $prec_comp = (float) $_POST['prec_comp'];
    $prec_vend = (float) $_POST['prec_vend'];
    $saldo = (float) $_POST['saldo'];
    $saldo_mini = (float) $_POST['saldo_mini'];

    $dados = ['descricao' => $descricao, 'unidade' => $unidade, 'prec_comp' => $prec_comp, 'prec_vend' => $prec_vend, 'saldo' => $saldo, 'saldo_mini' => $saldo_mini];


    $cadastro = new Produtos();
    $cadastro->cadastrar($dados);
    unset($_POST['']);
}
?>
                        </div>
                    </div>
                    <form action="" role="form" method="post">
                        <div class="row">
                            <br>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Descrição:</label> 
                                    <input type="text" class="form-control input-sm" name="descricao" value="">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Unidade:</label> 
                                    <select name="unidade" class="form-control input-sm">
                                        <option value="UN">UN</option>
                                        <option value="KG">KG</option>
                                        <option value="PR">PR</option>
                                        <option value="MT">MT</option>

                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Preço de Compra:</label> 
                                    <div class="input-group">
                                        <span class="input-group-addon input-sm">R$</span>
                                        <input type="text" class="form-control input-sm"  name="prec_comp">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Preço de Venda:</label> 
                                    <div class="input-group">
                                        <span class="input-group-addon input-sm">R$</span>
                                        <input type="text" class="form-control input-sm" name="prec_vend">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Saldo:</label> 
                                    <input type="text" class="form-control input-sm" name="saldo" value="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Saldo Min.:</label> 
                                    <input type="text" class="form-control input-sm" name="saldo_mini" value="">

                                </div>
                            </div>
                        </div>



                </div><!-- /.box-body -->

                <div class="box-footer">
                    <input type="submit" class=" btn btn-success" name="salvar" value="Salvar">

                    <button type="reset" class="btn btn-default">Limpar</button>

                    <button type="button" class="btn btn-danger">Cancelar</button>

                </div>
            </div>
            </form>
        </div><!-- /.box -->

    </div>
</div><!-- /.row -->
<!-- Main row -->


</section><!-- /.content -->
