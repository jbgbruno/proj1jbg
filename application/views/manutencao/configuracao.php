
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Configurações do Sistema
        <small>Algumas alterações não são reversiveis</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Configurações do Sistema</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="box box-danger  collapsed-box ">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ajuste de Estoque</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>

                                    </div><!-- /.box-header -->
                                    <!-- form start -->

                                    <div class="box-body">
                                        <form role="form">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Buscar Produto</label>
                                                        <div class="input-group">

                                                            <input type="text" name="message" placeholder="Type Message ..." class="form-control input-sm">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-sm btn-primary btn-flat"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Saldo Anterior:</label> 
                                                        <input type="text" class="form-control input-sm" name="nome" value=""disabled="disabled">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Quantidade para Adicionar:</label> 
                                                        <input type="text" class="form-control input-sm">
                                                    </div>
                                                </div>

                                            </div>


                                    </div><!-- /.box-body -->

                                    <div class="box-footer bg-gray-light">
                                        <button type="submit" class=" btn btn-success" >Salvar</button>

                                        <button type="button" class="btn btn-danger">Cancelar</button>

                                    </div>
                                </div>
                                </form>
                            </div><!-- /.box -->
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="box box-danger collapsed-box ">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ajuste de Preço de Produtos</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form">
                                        <div class="box-body">
                                            <div class="row">

                                                <div class="col-md-12">


                                                </div>
                                            </div>
                                            <div class="row">



                                                <div class="col-md-12">
                                                    <div class="alert alert-danger alert-dismissable">

                                                        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
                                                        Essa função afetar todo o seu estoque, pois ele altera os preços de todos os produtos cadastrados. Escolha se você vai adicionar ou dar desconto nos produtos. Não é possivel marcar as duas opções de uma vez.
                                                    </div>
                                                </div>   
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label>Adicionar percentual a todos os produtos:</label> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="percentPro" checked="">
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label>Reduzir percentual em todos os produtos:</label> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="percentPro">
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div><!-- /.box-body -->

                                        <div class="box-footer bg-gray-light">
                                            <button type="submit" class=" btn btn-primary" >Salvar</button>

                                            <button type="button" class="btn btn-danger">Cancelar</button>

                                        </div>
                                </div>
                                </form>
                            </div><!-- /.box -->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="box box-primary  collapsed-box ">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Unidades de Medida</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>

                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form">
                                        <div class="box-body">
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#listagem_unidade" data-toggle="tab" aria-expanded="true">Listagem</a></li>
                                                    <li class=""><a href="#cadastrar_unidade" data-toggle="tab" aria-expanded="false">Cadastro</a></li>



                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="listagem_unidade">
                                                        <table class="table table-bordered">
                                                            <tbody><tr class="bg-light-blue">
                                                                    <th style="width: 10px">#</th>
                                                                    <th>Nome</th>
                                                                    <th class="text-center col-md-2">Sigla</th>
                                                                    <th class="text-center col-md-2"> Excluir </th>
                                                                </tr>
                                                                <tr>
                                                                    <td>1.</td>
                                                                    <td>Unidade</td>
                                                                    <td>UN</td>

                                                                    <td class=" text-center">

                                                                        <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                                    </td>
                                                                </tr>

                                                            </tbody></table>
                                                    </div><!-- /.tab-pane -->
                                                    <div class="tab-pane" id="cadastrar_unidade">
                                                        <div class="row">

                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label>Nome da Unidade:</label> 
                                                                    <input type="text" class="form-control input-sm" name="nome" value="">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Sigla:</label> 
                                                                    <input type="text" class="form-control input-sm">
                                                                </div>
                                                            </div>
                                                         

                                                        </div>
                                                           <div class="box-footer bg-gray-light">
                                                                <button type="submit" class=" btn btn-success" >Salvar</button>

                                                                <button type="button" class="btn btn-danger">Cancelar</button>

                                                            </div>
                                                    </div><!-- /.tab-pane -->

                                                </div><!-- /.tab-content -->
                                            </div>


                                        </div><!-- /.box-body -->


                                </div>
                                </form>
                            </div><!-- /.box -->
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="box box-danger collapsed-box ">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ajuste de Preço de Serviços</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form">
                                        <div class="box-body">
                                            <div class="row">

                                                <div class="col-md-12">


                                                </div>
                                            </div>
                                            <div class="row">



                                                <div class="col-md-12">
                                                    <div class="alert alert-danger alert-dismissable">

                                                        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
                                                        Essa função afetar todo a sua relação de serviços, pois ele altera os preços de todos os serviços cadastrados. Escolha se você vai adicionar ou dar desconto nos serviços. Não é possivel marcar as duas opções de uma vez.
                                                    </div>
                                                </div>   
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label>Adicionar percentual a todos os serviços:</label> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="percentPro" checked="">
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label>Reduzir percentual em todos os serviços:</label> 
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="percentPro">
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div><!-- /.box-body -->

                                        <div class="box-footer bg-gray-light">
                                            <button type="submit" class=" btn btn-primary" >Salvar</button>

                                            <button type="button" class="btn btn-danger">Cancelar</button>

                                        </div>
                                </div>
                                </form>
                            </div><!-- /.box -->
                        </div>

                    </div><!-- /.box-body -->

                </form>
            </div>

        </div><!-- /.box -->


    </div>

    <!-- Main row -->


</section><!-- /.content -->
