<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Servicos extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('template/header');
        $this->load->view('servicos/listar-servicos');
        $this->load->view('template/footer');
    }
    public function cadastrar() {
        $this->load->view('template/header');
        $this->load->view('servicos/cadastro-servico');
        $this->load->view('template/footer');
    }

}
